variable "cloudflare_account_id" {
  description = "Account ID for CloudFlare"
}

variable "cloudflare_api_token" {
  description = "CloudFlare API Token"
}
